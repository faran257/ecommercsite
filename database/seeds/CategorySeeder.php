<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'uuid' => 'oPQ6Eu8HmA+YcA==',
                'name' => 'Mobiles'
            ],
            [
                'uuid' => 'vh16RCD/GRMNkQ==',
                'name' => 'Vehicles'
            ],
            [
                'uuid' => 'LK6ut4kH3QQOhg==',
                'name' => 'Property for Sale'
            ],
            [
                'uuid' => 'USOXBBg+J9UU0g==',
                'name' => 'Property for Rent'
            ],
            [
                'uuid' => 'DFTiqgqKlUGbFA==',
                'name' => 'Electronics & Home Appliances'
            ],
            [
                'uuid' => 'OeWRxPW8x6NZmQ==',
                'name' => 'Bikes'
            ],
            [
                'uuid' => '31BRWfnVrtfngw==',
                'name' => 'Business, Industrial & Agriculture'
            ],
            [
                'uuid' => 'FU8iggWe5X6JIQ==',
                'name' => 'Services'
            ],
            [
                'uuid' => 'aBUHqJEV0X7kXA==',
                'name' => 'Jobs'
            ],
            [
                'uuid' => '2Iy/SgoZQNdpLw==',
                'name' => 'Animals'
            ],
            [
                'uuid' => 'iZy2LEQBTJs8rg==',
                'name' => 'Furniture & Home Decor'
            ],
            [
                'uuid' => 'KPQ6Eu(HmA+YcA==',
                'name' => 'Fashion & Beauty'
            ],
            [
                'uuid' => 'YPQ6Eu8HJG+YcA==',
                'name' => 'Books, Sports & Hobbiesr'
            ]
        ]);
    }
}
