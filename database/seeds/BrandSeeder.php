<?php

use Illuminate\Database\Seeder;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('brands')->insert([
            [
                'uuid' => 'oPQ6Eu8HmA+YcA==',
                'name' => 'Dell'
            ],
            [
                'uuid' => 'vh16RCD/GRMNkQ==',
                'name' => 'HP'
            ],
            [
                'uuid' => 'LK6ut4kH3QQOhg==',
                'name' => 'Samsumg'
            ],
            [
                'uuid' => 'USOXBBg+J9UU0g==',
                'name' => 'Accer'
            ],
            [
                'uuid' => 'DFTiqgqKlUGbFA==',
                'name' => 'Toshiba'
            ],
            [
                'uuid' => 'OeWRxPW8x6NZmQ==',
                'name' => 'Oppo'
            ],
            [
                'uuid' => '31BRWfnVrtfngw==',
                'name' => 'Nokia'
            ]
        ]);
    }
}
