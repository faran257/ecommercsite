<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->coment('id from categorty');
            $table->string('name')->nullable();
            $table->string('product_image')->nullable();
            $table->unsignedBigInteger('category_id')->coment('id from categorty');
            $table->unsignedBigInteger('brand_id')->coment('id from brand');
            $table->unsignedBigInteger('product_price_id')->coment('id from brand');
            $table->string('product_uid')->unique()->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
