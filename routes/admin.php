<?php

use Illuminate\Support\Facades\Route;
use App\User;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//  Admin panel routes start
Route::get('/home', 'HomeController@index')->name('home');
Route::resource('/category','Admin\CategoryController');
Route::resource('/brand','Admin\BrandController');
Route::resource('/product','Admin\ProductController');
//  Admin panel routes end

Route::get('/shops/{id}','Shop\ShopController@index')->name('shop');

Route::get('/layout',function (){ return view('layout');})->name('layout');
Route::get('layout/{id}/{shop}','Shop\ShopController@updateLayout');