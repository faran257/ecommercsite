@extends('layouts.'.$users->shop_layout)

@section('content')
<div class="container" style="margin-top:30px;">

    <div class="row">

      <div class="col-lg-3">

        <h1 class="my-4">{{$users->name}}</h1>
        <div class="list-group">
          <a href="#" class="list-group-item">{{$users->email}}</a>
          <a href="#" class="list-group-item">{{$users->phone}}</a>
          
        </div>

      </div>
      <!-- /.col-lg-3 -->

      <div class="col-lg-9"> <!--contrairement à la section "prochainement", la section carousel propose les films à l'affiche-->

        <div class="row" style="margin-top:100px;">
            <div class="col-md-4">
              <div class="card" style="width: 18rem;">
                <img class="" src="{{$products->product_image}}" alt="Card image cap">
                <div class="card-body">
                  <h5 class="card-title">{{$products->name}}</h5>
                  <p class="card-text">{{$products->description}}</p>
                  <p class="card-text">Rs = {{$products->productPrice->sell_price}}</p>
                  <a href="#" class="btn btn-primary">Go somewhere</a>
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="card" style="width: 18rem;">
                <img class="" src="{{$products->product_image}}" alt="Card image cap">
                <div class="card-body">
                  <h5 class="card-title">{{$products->name}}</h5>
                  <p class="card-text">{{$products->description}}</p>
                  <p class="card-text">Rs = {{$products->productPrice->sell_price}}</p>
                  <a href="#" class="btn btn-primary">Go somewhere</a>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card" style="width: 18rem;">
                <img class="" src="{{$products->product_image}}" alt="Card image cap">
                <div class="card-body">
                  <h5 class="card-title">{{$products->name}}</h5>
                  <p class="card-text">{{$products->description}}</p>
                  <p class="card-text">Rs = {{$products->productPrice->sell_price}}</p>
                  <a href="#" class="btn btn-primary">Go somewhere</a>
                </div>
              </div>
            </div>
        </div>
        
        <!-- /.row -->

      </div>
      <!-- /.col-lg-9 -->

    </div>
    <!-- /.row -->

  </div>

@endsection