@extends('layouts.admin.admin')

@section('content')
<div class="container bg-white">
    <div class="row">
        <div class="col-md-12">
        <table id="example" class="table table-striped table-bordered" style="width:100%">
          <thead>
              <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Action</th>
              </tr>
          </thead>
          @foreach( $brands as $brand )
          <tbody>
             <tr> 
                <td>{{$brand->uuid}}</td>
                <td>{{$brand->name}}</td>
                <td> 
                   <div class="group pull-right">
                    <u><a href="">new sub-category</a></u> |
                    <u><a href="">upload picture</a></u>
                   <a href=""><button class="btn btn-sm btn-info">View</button></a>
                   <a  data-toggle="modal" data-target="#modalCategoryEdit"><button class="btn btn-sm btn-primary">Edit</button></a>
                   <a href=""><button class="btn btn-sm btn-danger">Del</button></a>
                   </div>
                </td>
             </tr>
          </tbody>
          @endforeach
        </table>
        </div>
    </div>
</div>


@endsection