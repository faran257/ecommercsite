@extends('layouts.admin.main')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3>Brands</h3>
                @if (session()->has('error'))
                    <div  class="alert alert-danger">
                        {{session()->get('error')}}
                    </div>
                @endif
                <div id="Message">
                    @if (session()->has('message'))
                        <div  class="alert alert-info">
                            {{session()->get('message')}}
                        </div>
                    @endif
                </div>
                <a href="{{route('brand.create')}}"><button class="btn btn-md btn-success pull-right">Add Brand</button></a>
                <div class="card">
                    <div class="card-header bg-orange text-white">
                       Brands
                    </div>

                    <div class="card-body">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            @foreach( $brands as $brand)
                                <tbody>
                                <tr>
                                    <td>{{$brand->uuid}}}</td>
                                    <td>{{$brand->name}}</td>
                                    <td>
                                        <a href="{{route('brand.edit',$brand->uuid)}}"><button class="btn btn btn-sm btn-info">Edit</button></a>
                                        <a href=""><button class="btn btn btn-sm btn-danger">Del</button></a>
                                    </td>
                                </tr>
                                </tbody>
                            @endforeach
                        </table>
                        {{$brands->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
