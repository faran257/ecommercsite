@extends('layouts.admin.admin')

@section('content')
<div class="container-fluid">
  <div class="row">
       <div class="col-md-12">
          <h3>Products</h3>
             <div class="card-header bg-dark text-white">
                  Products
                  <a href="{{route('product.create')}}"><button class="btn btn-success btn-md float-right">Add New</button></a>
             </div>
              <div class="card-body">
              <table id="example" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Image</th>
                            <th>Category</th>
                            <th>Brand</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>SellPrice</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    @foreach( $products as $product)
                    <tbody>
                      <tr>
                          <td>{{$product->product_uid}}</td>
                          <td><img src="{{$product->product_image}}" alt="" height="50" width="50"></td>
                          <td>{{$product->category->name}}</td>
                          <td>{{$product->brand->name}}</td>
                          <td>{{$product->name}}</td>
                          <td>{{$product->description}}</td>
                          <td>{{$product->productPrice->sell_price}}</td>
                          <td>
                              <a href=""><button class="btn btn btn-sm btn-info">Edit</button></a>
                              <a href=""><button class="btn btn btn-sm btn-danger">Del</button></a>
                          </td>
                      </tr>
                    </tbody>
                  @endforeach
                    </table>
              </div>
           </div>
       </div>
  </div>
</div>
@endsection
