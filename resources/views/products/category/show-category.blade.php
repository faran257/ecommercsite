@extends('layouts.admin.admin')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3>Category</h3>
                @if (session()->has('error'))
                    <div  class="alert alert-danger">
                        {{session()->get('error')}}
                    </div>
                @endif
                <div id="Message">
                @if (session()->has('message'))
                    <div  class="alert alert-info">
                        {{session()->get('message')}}
                    </div>
                @endif
                </div>
                
                <div class="card">
                    <div class="card-header bg-dark text-white">
                        Categories
                        <a href="{{route('category.create')}}"><button class="btn btn-md btn-success float-right">Add Category</button></a>
                    </div>

                    <div class="card-body">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            @foreach( $categories as $category)
                            <tbody>
                              <tr>
                                  <td>{{$category->uuid}}}</td>
                                  <td>{{$category->name}}</td>
                                  <td>
                                      <a href="{{route('category.edit',$category->uuid)}}"><button class="btn btn btn-sm btn-info">Edit</button></a>
                                      <a href=""><button class="btn btn btn-sm btn-danger">Del</button></a>
                                  </td>
                              </tr>
                            </tbody>
                            @endforeach
                        </table>
                        {{$categories->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
