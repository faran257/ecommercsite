@extends('layouts.admin.main')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3>Add Category Form</h3>
                <div class="card">
                    <div class="card-header bg-orange text-white">
                        Category Form
                    </div>
                    <div class="card-body">
                        <form  method="POST" action="{{route('category.store')}}">
                            {{csrf_field()}}
                            <div class="row justify-content-center">
                                <div class="col-sm-6">
                                    <input type="text" name="category" class="form-control" placeholder="category name" required>
                                </div>
                                <input type="submit" value="Save category">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
