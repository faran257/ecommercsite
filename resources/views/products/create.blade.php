@extends('layouts.admin.admin')

@section('content')
<div class="container-fluid">
  <div class="row">
       <div class="col-md-12">
          <h3>Add Products Form</h3>
            <div class="card">
                <div class="card-header bg-dark text-white">
                    Products Form
                </div>
                <div class="card-body">
                    <form  method="POST" action="{{route('product.store')}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="row">
							<div class="col-sm-6">
                                <select name="category" id="" class="form-control" value="{{ old('category') }}" required>
                                    <option value="">---choose category----</option>
                                    @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
							</div>
							<div class="col-sm-6">
                                <select name="brand" id="" class="form-control" value="{{ old('brand') }}"  required>
                                    <option value="">---choose brand---</option>
                                    @foreach($brands as $brand)
                                    <option value="{{$brand->id}}">{{$brand->name}}</option>
                                    @endforeach
                                </select>
							</div>
						</div>
                        <div class="row">
							<div class="col-sm-6">
                                <input type="text" class="form-control" value="{{ old('productid') }}"  name ="productid" placeholder="product id" required>
							</div>
                            <div class="col-sm-6">
                                <input type="text" name="productname" class="form-control" placeholder="product name" value="{{ old('productname') }}" placeholder="product name" required>
							</div>
						</div>
                        <div class="row">
							<div class="col-sm-6">
                                 <input type="text" class="form-control" value="{{ old('productid') }}"  name ="purchaseprice" placeholder="product purchase price" required>
							</div>
                            <div class="col-sm-6">
                                 <input type="text" name="sellprice" class="form-control" placeholder="product name" value="{{ old('productname') }}" placeholder="product sell price" required>
							</div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <input type="file" name="image"  required>
                            </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                              <textarea class="description form-control"  name="description" value="{{ old('description') }}" required>
                              </textarea>
                          </div>
                        </div>
                        <input type="submit" value="Save Product">
                    </form>
                </div>
            </div>
        </div>
  </div>
</div>
<script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
<script>
    tinymce.init({
        selector:'textarea.description',
        height: 300
    });
</script>
@endsection
