@extends('layouts.admin.main')

@section('content')
  <div class="container">
      <div class="row">
          <div class="col-md-4">
                <h3>Apply Layout Default </h3>
                 <a href="{{url('/layout/'.Auth::user()->id.'/shop-one')}}"><input type="button" value="Shop one"></a>
           </div>
           <div class="col-md-4">
                <h3>Apply Layout Shop Two</h3>
                 <a href="{{url('/layout/'.Auth::user()->id.'/shop-two')}}"><input type="button" value="Shop Two"></a>
           </div>
           <div class="col-md-4">
                <h3>Apply Layout Shop Three</h3>
                <a href=""><input type="button" value="Shop Three"></a>
           </div>
      </div>
  </div>
@endsection