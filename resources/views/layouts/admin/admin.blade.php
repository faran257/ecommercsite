<!DOCTYPE html>
<html lang="en">
<head>
  @include('include.admin.header')
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
          @include('page_contents.admin.navbar')
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
     @include('page_contents.admin.main-side-bar')
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
     @include('page_contents.admin.content-header')

    <!-- Main content -->
    <section class="content">
      @yield('content')
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2020 </strong>
    All rights reserved.
  
  </footer>
</div>
<!-- ./wrapper -->
@include('include.admin.footer')
</body>
</html>
