<?php

namespace App\Traits;
use Intervention\Image\ImageManagerStatic as Image;

trait UploadImageTrait
{
    /*
     * For upload Image 
     * */
    public function imagePhoto($request)
    {
        $avatar = $request->file($this->photo);
        $filename = time(). '.'.$avatar->getClientOriginalExtension();
        Image::make($avatar)->resize(300,300)->save(public_path('upload/'.$filename));
        $file_path = '/upload/'.$filename;
        return $file_path;
    }
}

?>