<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','product_image', 'category_id', 'brand_id', 'product_price_id', 'product_uid', 'name', 'description',
    ];

    /**
     * Get the categories.
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Get the Product .
     */
    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

     /**
     * Get the Product .
     */
    public function productPrice()
    {
        return $this->belongsTo(ProductPrice::class);
    }

    /**
     * Get the categories.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
