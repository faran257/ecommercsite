<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPrice extends Model
{
    protected  $table ='product_prices';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'purchase_price', 'sell_price',
    ];

     /**
     * Get the brands.
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
