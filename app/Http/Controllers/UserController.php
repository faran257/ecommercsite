<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class UserController extends Controller
{
    public function show()
    {
        return view('auth.register');
    }

    // public function authenticate(Request $request)
    // {
    //     $validator = $request->validate([
    //         'email'     => 'required',
    //         'password'  => 'required|min:6'
    //     ]);

    //     if (Auth::attempt($validator)) {
    //         return redirect()->route('dashboard');
    //     }
    // }

     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function register(Request $request)
    {
        $validator = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'phone' => ['required'],
            'country' => ['required'],
            'store_name' => ['required'],
            'subdomain' => ['required'],
            'business_type' => ['required'],
        ]);

        $users = new User;
        $users->fill($validator);
        $users->password = Hash::make($validator['password']);
        if ($users->save()) {
            echo "User created Successful";
        } else {
            echo "not store";
        }
     }


    //  public function logout()
    // {
    //     Session::flush()
    //     Auth::logout();
    //     return back();
    // }
}
