<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Traits\UploadImageTrait;
use App\Category;
use App\Brand;
use App\Product;
use App\ProductPrice;
use Image;
use Auth;

class ProductController extends Controller
{
    use UploadImageTrait;

    protected $photo = 'photo';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::with('category','brand','productPrice')->get();
        return view('products.manage-product',compact('products'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $brands = Brand::all();
        return view('products.create',compact('categories','brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $productPrices = ProductPrice::create([
            'purchase_price'=>$request->purchaseprice,
            'sell_price'=>$request->sellprice
        ]);
        if ($productPrices) {
        $products = new Product;
        $products->fill($request->all());
        if (!empty($request->image)){
            $image       = $request->file('image');
            $filename = time().mt_rand(100000, 999999).'.'.$image ->getClientOriginalExtension();
            Image::make($image)->save(public_path('upload/'.$filename)); // put in Job Queue
            $file_path = '/upload/'.$filename;
        }
        $products = Product::create([
            'user_id' => Auth::user()->id,
            'product_image' => $file_path,
            'category_id' => $request->category,
            'brand_id' => $request->brand,
            'name' => $request->productname,
            'product_uid' => $request->productid,
            'description' => $request->get('description'),
            'product_price_id' => $productPrices->id
        ]);
        if ($products) {
          return redirect('/product');
        }
      } else {
          return back();
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
