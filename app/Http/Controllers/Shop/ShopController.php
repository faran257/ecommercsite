<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Product;
use Session;

class ShopController extends Controller
{
    /**
     * Shop shows
     *
     * @param  array  $data
     * @return \App\User
     */
    public function index($id)
    {
        $users = User::find($id)->first();
        $products = Product::where('user_id',$id)->with('category','brand','productPrice')->first();
        return view('shop',compact('users','products'));
    }
     
    /**
     * Shop layout update
     *
     * @param  array  $data
     * @return \App\User
     */
    public function updateLayout($id, $shop)
    {
        $users = User::where('id',$id)->update(['shop_layout'=>$shop]);
        if ($users) {
            return back();
        } else{
            return back();
        }
    }
}
